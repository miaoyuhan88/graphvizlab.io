---
name: Portable Document Format (PDF)
params:
- pdf
---
Produces [PDF](http://www.adobe.com/devnet/pdf/) output.
(This option assumes Graphviz includes the Cairo renderer.)
Alternatively, one can use the [ps2](#d:ps2) option to
produce PDF-compatible PostScript, and then use a ps-to-pdf converter.
