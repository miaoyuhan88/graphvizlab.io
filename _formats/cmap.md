---
name: Client-side imagemap (deprecated)
params:
- cmap
---
Produces map files for client-side image maps. The cmap format is
mostly identical to cmapx, but the latter is well-formed XML amenable
to processing by XML tools. In particular, the cmapx output is wrapped in
`<map></map>`.

See [Note](#ID).
