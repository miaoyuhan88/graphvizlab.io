---
name: POV-Ray markup language (prototype)
params:
- pov
---
Scene-description language for 3D modelling for the 
[Persistence of Vision Raytracer](http://www.povray.org/).
