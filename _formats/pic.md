---
name: 'Kernighan''s PIC graphics language '
params:
- pic
---
Output is given in the text-based PIC language developed for troff.
See [Pic language](https://en.wikipedia.org/wiki/Pic_language).
