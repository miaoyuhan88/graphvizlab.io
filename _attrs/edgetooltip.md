---
defaults:
- '""'
flags:
- cmap
- svg
minimums: []
name: edgetooltip
types:
- escString
used_by: E
---
Tooltip annotation attached to the non-label part of an edge.
