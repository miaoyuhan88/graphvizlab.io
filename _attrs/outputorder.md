---
defaults:
- breadthfirst
flags: []
minimums: []
name: outputorder
types:
- outputMode
used_by: G
---
Specify order in which nodes and edges are drawn.
