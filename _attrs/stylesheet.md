---
defaults:
- '""'
flags:
- svg
minimums: []
name: stylesheet
types:
- string
used_by: G
---
A URL or pathname specifying an XML style sheet, used in SVG output.

Combine with [`class`](#d:class) to style elements using CSS selectors.
