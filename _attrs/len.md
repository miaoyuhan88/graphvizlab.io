---
defaults:
- 1.0(neato)
- 0.3(fdp)
flags:
- neato
- fdp
minimums: []
name: len
types:
- double
used_by: E
---
Preferred edge length, in inches.
