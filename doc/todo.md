---
layout: page
title: Graphviz Wish List
redirect_from:
- /_pages/doc/todo.html
---
The Graphviz todo list has moved to the [Graphviz issue tracker](https://gitlab.com/graphviz/graphviz/-/issues).
