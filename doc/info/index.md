---
layout: page
title: GraphViz Reference
---

- [The DOT Language](lang.html)
- [Command-line Usage](command.html)
- [Output Formats](output.html)
- [Graph Attributes](attrs.html)
- [Node Shapes](shapes.html)
- [Colors](colors.html)
