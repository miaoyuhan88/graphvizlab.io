---
name: lblString
---
An [escString](#k:escString) or an [HTML label](shapes.html#html).
